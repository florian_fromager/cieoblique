<footer>
    <?php echo scotch(); ?>
    <script>
        Modernizr.on( 'webp', function ( result ) {
            if ( result ) { document.querySelector( '#scotch img' ).src.replace( /webp/g, "jpg" ); }
        });
    </script>
</footer>
</div>

<?php wp_footer(); ?>
<?php
    $detect = detect();
    if ( !$detect->isMobile() && !$detect->isTablet() ) echo '<svg id="fleche" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 110.55 155.91"><path d="M56.32 0h-2.08a42.87 42.87 0 0 1-9.77 16.52C31.58 30.35 13.17 33.25 0 33.25v2c36 0 49.74-20.11 54.28-29.84v150.5h2V5.53a47.63 47.63 0 0 0 8.27 12.28c13.38 14.41 32.4 17.44 46 17.44v-2C66.86 33.25 57.14 2.88 56.32 0Z"/></svg>';
?>
<script src="<?php echo get_home_url() ?>/wp-content/themes/cieoblique/assets/dist/main.js"></script>

</body>
</html>
