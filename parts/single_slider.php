<?php 
    $slug = $args[ 'slug' ];
    $videos = $args[ 'videos' ];
?>
<aside id="aside" class="<?php echo $slug ?>__aside">
<?php

    $gallery = get_field( $slug.'_gallery' );

    if ( !empty( $gallery ) ) {

?>
    <div class="splide">
        <div class="splide__track">
            <ul class="splide__list">

            <?php foreach( $gallery as $key => $value ) {
            
                $src = wp_get_attachment_image_src( $value->ID, "medium-large", null );
                $srcLazy = wp_get_attachment_image_src( $value->ID, "large", null );
                
            ?><li class="splide__slide<?php if( $key == 0 ) echo " is-first"; ?>">
                    <div>
                        <img
                            src="<?php echo $src[ 0 ]; ?>"
                            data-splide-lazy="<?php echo $srcLazy[ 0 ]; ?>"
                            alt="<?php echo $value->post_title; ?>" >
                    </div>
                </li>
            <?php } ?>

            </ul>
        </div>
    </div>
<?php

    }; if ( scan( $videos ) ) {

?>
    <div class="<?php echo $slug ?>__videos hidden">

        <button id="videos__btn--close" class="btn">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72.99 72.99"><path d="M72.99 2.12 70.87 0 36.49 34.37 2.12 0 0 2.12l34.37 34.37L0 70.87l2.12 2.12 34.37-34.37 34.38 34.37 2.12-2.12-34.37-34.38L72.99 2.12z"/></svg>
        </button>

        <div class="splide">
            <div class="splide__track">
                <ul class="splide__list">
            <?php
                for ( $i = 1; $i < 5; $i++ ) {

                    $field = $slug."_videos_".$i;
                    if ( !empty( $videos[ $field ] ) ) {
                
            ?>
                    <li class="splide__slide<?php if( $i == 1 ) echo " is-first"; ?>">
                        <div><?php echo $videos[ $field ]; ?></div>
                    </li>
            <?php
                    }
                }
            ?>
                </ul>
            </div>
        </div>
    </div>
<?php } ?>
</aside>