<aside id="aside" class="home__aside">
<?php

$actus = $args[ 'actus' ];

$gallery = get_field( 'home_gallery' );

if ( !empty( $gallery ) ) {

?>
<div class="splide">
    <div class="splide__track">
        <ul class="splide__list">
        <?php foreach( $gallery as $key => $value ) {
            
            $src = wp_get_attachment_image_src( $value->ID, "medium-large", null );
            $srcLazy = wp_get_attachment_image_src( $value->ID, "large", null );
            
        ?><li class="splide__slide<?php if( $key == 0 ) echo " is-first"; ?>">
                <div>
                    <img
                        src="<?php echo $src[ 0 ]; ?>"
                        data-splide-lazy="<?php echo $srcLazy[ 0 ]; ?>"
                        alt="<?php echo $value->post_title; ?>" >
                </div>
            </li>
        <?php } ?>
        </ul>
    </div>
</div>
<?php 
    $detect = detect();
    if ( !$detect->isMobile() ) {

        for ( $i = 0; $i < count( $actus ); $i++ ) {

            $actu = $actus[ $i ];
            $id = $actu->ID;
            $title = $actu->post_title;
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'medium_large' );
?>
    <figure class="fig-<?php echo $i; ?> hidden">
        <img src="<?php echo $thumb[ 0 ]; ?>" alt="<?php echo $title; ?>" loading="lazy" srcset="" sizes="(max-width: 768px) 100vw, 768px" width="768" height="1041" >
    </figure>
<?php
        };

    };

};
?>
</aside>