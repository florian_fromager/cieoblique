<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'obliquecom');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'obliquecom');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'pH735PXjsu8R');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*1Xe+uVE+aiN:sw=$m+30|2zT,ei#V(w<E`Ona^cZ||M2VP`dzs;Zw;aS-yJH=-%');
define('SECURE_AUTH_KEY',  'V<:K|!=}vMNV9Ttn})F9q+I*Z-!d,|QJdB8&#X)CI+Tj>xrOk|#w%?U/4=1Ypl1-');
define('LOGGED_IN_KEY',    '$i<@.Lbj45^LsEd<-F&-Cqk(}xbI+qKz[vP/gC2ALp/Vc4b(A#-;:$]tV26#0x/e');
define('NONCE_KEY',        '$K>l(gVqk!`KllW*TIMhwmA6|t:2p3<e6@N|G{X8x[^-T4u8phD17UyYas[ws-r6');
define('AUTH_SALT',        'f$+SR}Om+pj<*=E4I+Jl6LO#.hC]`[~6+TqxppE+fH|`3p_&mdDQrtG=.-N9mXgj');
define('SECURE_AUTH_SALT', 'G>!fj)&GhyB-.R%K@7j#$YiO<|5XiX78ge{!3d(]i&X<+wh5j1<0e.<~Xq1v)UL-');
define('LOGGED_IN_SALT',   'SHV-f?+-|,g:NdW5~>l@){tIR<D.; ~8GI_};]wHi:8A*YF-oT[xkOJD&cv%!g+!');
define('NONCE_SALT',       '+tlCF*b-^1+7Yd=($XGv{^q~^P1B-_s}3lS+%1uHsBkL#|Zw;(,N=3vg}SWJcUoN');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'djhs_';

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
//Disable File Edits
define('DISALLOW_FILE_EDIT', true);