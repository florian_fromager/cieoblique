import Pjax from './pjax.esm.js';
import Splide from '@splidejs/splide';

const cursor = document.querySelector( '#fleche' );

const is = ( el, arr ) => {

    var result = false;

    for ( let i = 0; i < arr.length; i++ ) {

        result = el.classList.contains( arr[ i ] ) ? true : result;

    }

    return result;

}, cursorMove = e => {
      
    cursor.style.top = `${ e.pageY }px`;
    cursor.style.left = `${ e.pageX }px`;
  
}, prevNext = () => {

    let arrows = document.querySelectorAll( '.splide__arrow' );
    for (let i = 0; i < arrows.length; i++) {
        const arrow = arrows[ i ];
        arrow.addEventListener( "mouseenter", () => {
            cursor.style.opacity = 1;
            cursor.style.transform = is( arrow, [ 'splide__arrow--next' ] ) ? 'translate(-50%,-50%) rotate(180deg)' : 'translate(-50%,-50%)';
        } );
        arrow.addEventListener( "mouseleave", () => cursor.style.opacity = 0 );
    }

}, orthoTypo = str => {

    const stArr = [
        [ ' ;', '&nbsp;;' ],
        [ ' :', '&nbsp;:' ],
        [ ' !', '&nbsp;!' ],
        [ ' ?', '&nbsp;?' ],
        [ '« ', '«&nbsp;' ],
        [ ' »', '&nbsp;»' ],
    ];

    for ( let i = 0; i < stArr.length; i++ ) {
        const e = stArr[ i ];
        if ( str.includes( e[ 0 ] ) ) str = str.replaceAll( e[ 0 ], e[ 1 ] );
    }

    return str;

}, groupSize = main => {

    const content = document.querySelector( '#content' );

    if ( is( content, [ 'creation', 'transmission' ] ) ) {

        let innerContainers = document.querySelectorAll( '.wp-block-group__inner-container' );
        for (let i = 0; i < innerContainers.length; i++) {
            const container = innerContainers[ i ];
            if ( container.offsetHeight === 0 ) {
                container.parentElement.remove();
            }
        }

        let columnsContainer = document.querySelectorAll( '.wp-block-columns' );
        for ( let i = 0; i < columnsContainer.length; i++ ) {
            const container = columnsContainer[ i ];
            const columns = container.querySelectorAll( '.wp-block-column' );
            let emptyColumns = true;
            for ( let j = 0; j < columns.length; j++ ) {
                emptyColumns = columns[ j ].offsetHeight !== 0 ? false : emptyColumns;
            }
            if ( emptyColumns === true ) container.remove();
        }

    }

    if ( is( content, [ 'creation', 'transmission', 'contacts' ] ) ) {

        const title = main.querySelector( "h1" );
        const notTitle = document.querySelectorAll( "#main > *:not( .main__heading ):not( #videos__btn--open )" );

        if ( title !== null ) {

            for ( let i = 0; i < notTitle.length; i++ ) {

                const e = notTitle[ i ];
                e.style.minHeight = `calc( 100vh - ${ title.offsetHeight + 150 }px )`;

            }

        } else {

            for ( let i = 0; i < notTitle.length; i++ ) {

                notTitle[ i ].style.minHeight = `calc( 100% + 2px )`;

            }

        }

    }

    if ( is( content, [ 'compagnie' ] ) && !is( document.body, [ "mobile" ] ) ) {

        // const cat = main.querySelector( ".main__category" );
        const title = main.querySelector( ".main__category + .wp-block-group" );
        const notTitle = document.querySelectorAll( "#main > *:not( .main__category ):not( .main__category + .wp-block-group )" );

        if ( title !== null ) {

            for ( let i = 0; i < notTitle.length; i++ ) {

                const e = notTitle[ i ];
                e.style.minHeight = `calc( 100vh - ${ title.offsetHeight + 150 }px )`;

            }

        }

    }

}, titleHeight = () => {

    const videos = document.querySelector( '.creation__videos' );
    if ( videos ) {

        const headers = document.querySelectorAll( 'header' );
        let height = 0;

        for ( let i = 0; i < headers.length; i++ ) {
            const e = headers[ i ];
            height = height + e.offsetHeight;
        }

        document.documentElement.style.setProperty( '--top', `${ height }px` );
        
    }

}, navClick = () => {

    const details = document.querySelector( 'header details' );
    const items = details.querySelectorAll( '.menu-item' );
    if ( items ) {

        for ( let i = 0; i < items.length; i++ ) {
            const e = items[ i ];
            e.addEventListener( "click", function() {
                details.removeAttribute( "open" );
            });
        }
        
    }

    document.querySelector( 'body > header > a' ).addEventListener( "click", function() {
        details.removeAttribute( "open" );
    });

}, pageHover = main => {

    const links = main.querySelectorAll( "article" );
    for ( let i = 0; i < links.length; i++ ) {
        const link = links[ i ];
        const linkId = link.dataset.id;
        link.addEventListener( "mouseenter", function() {
            document.querySelector( `.fig-${ linkId }` ).classList.remove( "hidden" );
        });
        link.addEventListener( "mouseleave", function() {
            document.querySelector( `.fig-${ linkId }` ).classList.add( "hidden" );
        });
    }

}, singleSplide = singleSplides => {

    for ( let i = 0; i < singleSplides.length; i++ ) {
        
        var splide = new Splide( singleSplides[ i ], {
            type: 'loop',
            direction: 'ttb',
            height: '100vh',
            pagination: false,
            arrows: 'slider',
            lazyLoad: 'nearby',
            speed: 250
        } );

        splide.mount();
        
    }

}, videoSplide = videos => {

    var splide = new Splide( videos.querySelector( ".splide" ), {
        type: 'loop',
        direction: 'ttb',
        height: '100vh',
        pagination: false,
        arrows: 'slider'
    } );

    splide.mount();

    let videos_btn = document.querySelectorAll( ".btn" );
    if (videos_btn) {
        for (let i = 0; i < videos_btn.length; i++) {
            const btn = videos_btn[i];
            btn.addEventListener( "click", function() {
                videos.classList.toggle( "hidden" );
            });
        }
    }

}, homeSplide = homeAside => {

    var homeSplide = new Splide( homeAside.querySelector( ".splide" ), {
        type: 'loop',
        direction: 'ttb',
        height: '100vh',
        pagination: false,
        arrows: false,
        autoplay: true,
        lazyLoad: 'nearby',
        speed: 250
    } );

    homeSplide.mount();

}, init = () => {

    setTimeout(() => {

        const main = document.querySelector( "#main" );

        const sentences = document.querySelectorAll( "p" );
        for ( let i = 0; i < sentences.length; i++ ) {
            const e = sentences[ i ];
            const sentence = e.innerHTML;
            e.innerHTML = orthoTypo( sentence );
        }

        const singleSplides = document.querySelectorAll( "#aside:not( .home__aside ) > .splide" );
        if ( singleSplides ) singleSplide( singleSplides );

        const creation_videos = document.querySelector( ".creation__videos" );
        if( creation_videos ) videoSplide( creation_videos );

        const transition_videos = document.querySelector( ".transmissions__videos" );
        if( transition_videos ) videoSplide( transition_videos );

        if ( !is( document.body, [ "mobile" ] ) ) {
            if ( is( main, [ "home__main", "spectacles__main", "transmissions__main", "calendrier__main" ] ) ) pageHover( main );

            if ( singleSplides.length !== 0 || creation_videos !== null || transition_videos !== null ) {
                const aside = document.querySelector( "#aside" );
                if ( aside ) {
                    aside.addEventListener( "mousemove", e => cursorMove( e ) );
                    prevNext();
                }
            }

        } else {

            document.documentElement.style.overflow = "initial";

            navClick();

            if ( is( main, [ "home__main", "creation__main", "transmission__main" ] ) ) titleHeight();

        }

        const homeAside = document.querySelector( ".home__aside" );
        if ( homeAside ) homeSplide( homeAside );

        groupSize( main );

        document.querySelector( '#content' ).classList.remove( 'loading' );

    }, 400);

}, pjax = new Pjax( {

    elements: "a",
    selectors: [ "#content" ],

} );

document.addEventListener( 'pjax:send', () => document.querySelector( "#content" ).classList.add( "fadeout" ) );

document.addEventListener( 'pjax:success', () => init() );

window.addEventListener( 'load', event => init() );