"use strict";

var _pjaxEsm = _interopRequireDefault(require("./pjax.esm.js"));

var _splide = _interopRequireDefault(require("@splidejs/splide"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var cursor = document.querySelector('#fleche');

var is = function is(el, arr) {
  var result = false;

  for (var i = 0; i < arr.length; i++) {
    result = el.classList.contains(arr[i]) ? true : result;
  }

  return result;
},
    cursorMove = function cursorMove(e) {
  cursor.style.top = "".concat(e.pageY, "px");
  cursor.style.left = "".concat(e.pageX, "px");
},
    prevNext = function prevNext() {
  var arrows = document.querySelectorAll('.splide__arrow');

  var _loop = function _loop(i) {
    var arrow = arrows[i];
    arrow.addEventListener("mouseenter", function () {
      cursor.style.opacity = 1;
      cursor.style.transform = is(arrow, ['splide__arrow--next']) ? 'translate(-50%,-50%) rotate(180deg)' : 'translate(-50%,-50%)';
    });
    arrow.addEventListener("mouseleave", function () {
      return cursor.style.opacity = 0;
    });
  };

  for (var i = 0; i < arrows.length; i++) {
    _loop(i);
  }
},
    orthoTypo = function orthoTypo(str) {
  var stArr = [[' ;', '&nbsp;;'], [' :', '&nbsp;:'], [' !', '&nbsp;!'], [' ?', '&nbsp;?'], ['« ', '«&nbsp;'], [' »', '&nbsp;»']];

  for (var i = 0; i < stArr.length; i++) {
    var e = stArr[i];
    if (str.includes(e[0])) str = str.replaceAll(e[0], e[1]);
  }

  return str;
},
    groupSize = function groupSize(main) {
  var content = document.querySelector('#content');

  if (is(content, ['creation', 'transmission'])) {
    var innerContainers = document.querySelectorAll('.wp-block-group__inner-container');

    for (var i = 0; i < innerContainers.length; i++) {
      var container = innerContainers[i];

      if (container.offsetHeight === 0) {
        container.parentElement.remove();
      }
    }

    var columnsContainer = document.querySelectorAll('.wp-block-columns');

    for (var _i = 0; _i < columnsContainer.length; _i++) {
      var _container = columnsContainer[_i];

      var columns = _container.querySelectorAll('.wp-block-column');

      var emptyColumns = true;

      for (var j = 0; j < columns.length; j++) {
        emptyColumns = columns[j].offsetHeight !== 0 ? false : emptyColumns;
      }

      if (emptyColumns === true) _container.remove();
    }
  }

  if (is(content, ['creation', 'transmission', 'contacts'])) {
    var title = main.querySelector("h1");
    var notTitle = document.querySelectorAll("#main > *:not( .main__heading ):not( #videos__btn--open )");

    if (title !== null) {
      for (var _i2 = 0; _i2 < notTitle.length; _i2++) {
        var e = notTitle[_i2];
        e.style.minHeight = "calc( 100vh - ".concat(title.offsetHeight + 150, "px )");
      }
    } else {
      for (var _i3 = 0; _i3 < notTitle.length; _i3++) {
        notTitle[_i3].style.minHeight = "calc( 100% + 2px )";
      }
    }
  }

  if (is(content, ['compagnie']) && !is(document.body, ["mobile"])) {
    // const cat = main.querySelector( ".main__category" );
    var _title = main.querySelector(".main__category + .wp-block-group");

    var _notTitle = document.querySelectorAll("#main > *:not( .main__category ):not( .main__category + .wp-block-group )");

    if (_title !== null) {
      for (var _i4 = 0; _i4 < _notTitle.length; _i4++) {
        var _e = _notTitle[_i4];
        _e.style.minHeight = "calc( 100vh - ".concat(_title.offsetHeight + 150, "px )");
      }
    }
  }
},
    titleHeight = function titleHeight() {
  var videos = document.querySelector('.creation__videos');

  if (videos) {
    var headers = document.querySelectorAll('header');
    var height = 0;

    for (var i = 0; i < headers.length; i++) {
      var e = headers[i];
      height = height + e.offsetHeight;
    }

    document.documentElement.style.setProperty('--top', "".concat(height, "px"));
  }
},
    navClick = function navClick() {
  var details = document.querySelector('header details');
  var items = details.querySelectorAll('.menu-item');

  if (items) {
    for (var i = 0; i < items.length; i++) {
      var e = items[i];
      e.addEventListener("click", function () {
        details.removeAttribute("open");
      });
    }
  }

  document.querySelector('body > header > a').addEventListener("click", function () {
    details.removeAttribute("open");
  });
},
    pageHover = function pageHover(main) {
  var links = main.querySelectorAll("article");

  var _loop2 = function _loop2(i) {
    var link = links[i];
    var linkId = link.dataset.id;
    link.addEventListener("mouseenter", function () {
      document.querySelector(".fig-".concat(linkId)).classList.remove("hidden");
    });
    link.addEventListener("mouseleave", function () {
      document.querySelector(".fig-".concat(linkId)).classList.add("hidden");
    });
  };

  for (var i = 0; i < links.length; i++) {
    _loop2(i);
  }
},
    singleSplide = function singleSplide(singleSplides) {
  for (var i = 0; i < singleSplides.length; i++) {
    var splide = new _splide["default"](singleSplides[i], {
      type: 'loop',
      direction: 'ttb',
      height: '100vh',
      pagination: false,
      arrows: 'slider',
      lazyLoad: 'nearby',
      speed: 250
    });
    splide.mount();
  }
},
    videoSplide = function videoSplide(videos) {
  var splide = new _splide["default"](videos.querySelector(".splide"), {
    type: 'loop',
    direction: 'ttb',
    height: '100vh',
    pagination: false,
    arrows: 'slider'
  });
  splide.mount();
  var videos_btn = document.querySelectorAll(".btn");

  if (videos_btn) {
    for (var i = 0; i < videos_btn.length; i++) {
      var btn = videos_btn[i];
      btn.addEventListener("click", function () {
        videos.classList.toggle("hidden");
      });
    }
  }
},
    homeSplide = function homeSplide(homeAside) {
  var homeSplide = new _splide["default"](homeAside.querySelector(".splide"), {
    type: 'loop',
    direction: 'ttb',
    height: '100vh',
    pagination: false,
    arrows: false,
    autoplay: true,
    lazyLoad: 'nearby',
    speed: 250
  });
  homeSplide.mount();
},
    init = function init() {
  setTimeout(function () {
    var main = document.querySelector("#main");
    var sentences = document.querySelectorAll("p");

    for (var i = 0; i < sentences.length; i++) {
      var e = sentences[i];
      var sentence = e.innerHTML;
      e.innerHTML = orthoTypo(sentence);
    }

    var singleSplides = document.querySelectorAll("#aside:not( .home__aside ) > .splide");
    if (singleSplides) singleSplide(singleSplides);
    var creation_videos = document.querySelector(".creation__videos");
    if (creation_videos) videoSplide(creation_videos);
    var transition_videos = document.querySelector(".transmissions__videos");
    if (transition_videos) videoSplide(transition_videos);

    if (!is(document.body, ["mobile"])) {
      if (is(main, ["home__main", "spectacles__main", "transmissions__main", "calendrier__main"])) pageHover(main);

      if (singleSplides.length !== 0 || creation_videos !== null || transition_videos !== null) {
        var aside = document.querySelector("#aside");

        if (aside) {
          aside.addEventListener("mousemove", function (e) {
            return cursorMove(e);
          });
          prevNext();
        }
      }
    } else {
      document.documentElement.style.overflow = "initial";
      navClick();
      if (is(main, ["home__main", "creation__main", "transmission__main"])) titleHeight();
    }

    var homeAside = document.querySelector(".home__aside");
    if (homeAside) homeSplide(homeAside);
    groupSize(main);
    document.querySelector('#content').classList.remove('loading');
  }, 400);
},
    pjax = new _pjaxEsm["default"]({
  elements: "a",
  selectors: ["#content"]
});

document.addEventListener('pjax:send', function () {
  return document.querySelector("#content").classList.add("fadeout");
});
document.addEventListener('pjax:success', function () {
  return init();
});
window.addEventListener('load', function (event) {
  return init();
});