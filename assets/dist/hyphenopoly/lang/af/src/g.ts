export const ao: i32 = 2018;
export const bm: i32 = 2170;
export const cm: i32 = 8276;
export const hv: i32 = 32698;
export const vm: i32 = 35751;
export const va: i32 = 40121;
export const lm: i32 = 1;
export const rm: i32 = 2;
