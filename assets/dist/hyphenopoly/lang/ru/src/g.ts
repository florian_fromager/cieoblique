export const ao: i32 = 2011;
export const bm: i32 = 2147;
export const cm: i32 = 4777;
export const hv: i32 = 15292;
export const vm: i32 = 16607;
export const va: i32 = 19898;
export const lm: i32 = 2;
export const rm: i32 = 2;
