export const ao: i32 = 2010;
export const bm: i32 = 2122;
export const cm: i32 = 4749;
export const hv: i32 = 15255;
export const vm: i32 = 16569;
export const va: i32 = 19056;
export const lm: i32 = 2;
export const rm: i32 = 3;
