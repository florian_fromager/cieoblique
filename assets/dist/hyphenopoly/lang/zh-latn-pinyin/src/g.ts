export const ao: i32 = 2147;
export const bm: i32 = 2347;
export const cm: i32 = 2528;
export const hv: i32 = 3247;
export const vm: i32 = 3337;
export const va: i32 = 3597;
export const lm: i32 = 1;
export const rm: i32 = 2;
