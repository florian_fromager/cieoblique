export const ao: i32 = 2040;
export const bm: i32 = 2144;
export const cm: i32 = 5625;
export const hv: i32 = 19546;
export const vm: i32 = 21287;
export const va: i32 = 24627;
export const lm: i32 = 2;
export const rm: i32 = 3;
