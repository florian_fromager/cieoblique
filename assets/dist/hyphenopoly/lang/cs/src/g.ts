export const ao: i32 = 1996;
export const bm: i32 = 2160;
export const cm: i32 = 3480;
export const hv: i32 = 8755;
export const vm: i32 = 9415;
export const va: i32 = 10819;
export const lm: i32 = 2;
export const rm: i32 = 3;
