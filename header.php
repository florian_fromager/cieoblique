
<?php

	$home = get_home_url();

	$homeID = get_option( "page_on_front" );
	$maintenance = get_field( "home_maintenance", $homeID );

	if ( $maintenance !== NULL && !is_user_logged_in() ) {

		if( $maintenance !== "false" && !is_front_page() ) {

			header( "Location: $home" );
			header( "Connection: close" );
			exit( 0 );

		}

	}

	$detect = detect();
	$mobile = $detect->isMobile();
	$tablet = $detect->isTablet();

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo "$home/wp-content/themes/cieoblique/assets/img/favicon" ?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo "$home/wp-content/themes/cieoblique/assets/img/favicon" ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo "$home/wp-content/themes/cieoblique/assets/img/favicon" ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo "$home/wp-content/themes/cieoblique/assets/img/favicon" ?>/site.webmanifest">
	<link rel="mask-icon" href="<?php echo "$home/wp-content/themes/cieoblique/assets/img/favicon" ?>/safari-pinned-tab.svg" color="#7eb4b4">
	<meta name="msapplication-TileColor" content="#7eb4b4">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>

	<script>
		var Hyphenopoly = {
			require: {
				"fr": "anticonstitutionnellement"
			},
			paths: {
				patterndir: "./patterns/",
				maindir: "./"
			},
			setup: {
				selectors: {
					".container": {}
				}
			}
		};
	</script>
	<script src="<?php echo "$home/wp-content/themes/cieoblique/assets/dist/hyphenopoly/Hyphenopoly_Loader.js" ?>"></script>
	<script src="<?php echo "$home/wp-content/themes/cieoblique/assets/dist/modernizr.min.js" ?>"></script>

    <link rel="stylesheet" href="https://use.typekit.net/cbd7fbh.css">
    <link rel="stylesheet" href="<?php echo $home ?>/wp-content/themes/cieoblique/assets/css/style.min.css">
	<?php echo $mobile ? "<link rel='stylesheet' href='$home/wp-content/themes/cieoblique/assets/css/mobile.min.css'>" : null ?>
</head>
<body <?php
	if ( $mobile || $tablet ) {
		
		$class = !$mobile ? null : 'class="';
		$class.= !$mobile ? null : ( $tablet ? "mobile tablet" : "mobile" );
		$class.= !$mobile ? null : '"';
		
		if( $class!= null ) echo $class;
			
	}
?>>
<?php wp_body_open(); ?>

	<?php get_template_part( $mobile ? 'parts/mobile_nav' : 'parts/nav' ); ?>