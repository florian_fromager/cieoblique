<?php

    get_header();

	$detect = detect();
    $mobile = $detect->isMobile();
    $tablet = $detect->isTablet();
    
    if ( have_posts() ) :
        
        $actus = get_field( "home_actus" );
        if( !empty( $actus ) ) {

?>	
<div id="content" class="home loading">
    
    <?php if( $mobile || $tablet ) get_template_part( 'parts/home_slider', null, array( 'actus' => $actus ) ); ?>

    <main id="main" class="home__main" role="main">

        <?php
            while (  have_posts() ) : the_post();

                the_content();

            endwhile;

            if ( count( $actus ) > 0 ) {
        ?>
        
        <h6>Actualités</h6>
        <?php
            for ( $i = 0; $i < count( $actus ); $i++ ) {

                $actu = $actus[ $i ];
                $title = $actu->post_title;
                $id = $actu->ID;
                $url = get_permalink( $id );
                $cat = get_the_category( $id );
                $cat = $cat[ 0 ];
                $slug = $cat->slug;
                $date = get_field( $slug."_annee", $id );
                $date = !empty( $date ) ? $date : date( "Y" );

                echo "<article data-id='$i'><a href='$url'><h4>$title, $slug $date</h4></a></article>";
                
            }
        ?>
        
    <?php
            }

        }
    ?>

    </main>
    
    <?php if( !$mobile && !$tablet ) get_template_part( 'parts/home_slider', null, array( 'actus' => $actus ) ); ?>

<?php else :

    _e( 'Sorry, no  posts matched your criteria.', 'textdomain' );

endif;

get_footer();

?>