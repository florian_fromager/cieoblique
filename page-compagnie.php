<?php

get_header();

if ( have_posts() ) :

?>
<div id="content" class="compagnie loading">

<main id="main" class="compagnie__main" role="main">

<?php
    
    while (  have_posts() ) : the_post();

        the_content();

    endwhile;

?>

</main>

<?php
    $detect = detect();
    if ( !$detect->isMobile() ) {
?>
<aside id="aside" class="compagnie__aside">
<?php
        $gallery = get_field( 'home_gallery' );
        if ( !empty( $gallery ) ) {
?>
<aside id="aside" class="compagnie__aside">
    <div class="splide">
        <div class="splide__track">
            <ul class="splide__list">
            <?php foreach( $gallery as $key => $value ) {
            
                $src = wp_get_attachment_image_src( $value->ID, "medium-large", null );
                $srcLazy = wp_get_attachment_image_src( $value->ID, "large", null );
                
            ?><li class="splide__slide<?php if( $key == 0 ) echo " is-first"; ?>">
                    <div>
                        <img
                            src="<?php echo $src; ?>"
                            data-splide-lazy="<?php echo $srcLazy; ?>"
                            alt="<?php echo $value->post_title; ?>" >
                    </div>
                </li>
            <?php } ?>
            </ul>
        </div>
    </div>
<?php 

        for ( $i = 0; $i < count( $actus ); $i++ ) {

            $id = $actu->ID;
            $actu = $actus[ $i ];
            $title = $actu->post_title;
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'medium_large');
        
?>
    <figure class="fig-<?php echo $i; ?> hidden">
        <img src="<?php echo $thumb[0]; ?>" alt="<?php echo $title; ?>" loading="lazy" srcset="" sizes="(max-width: 768px) 100vw, 768px" width="768" height="1041">
    </figure>
<?php
            };

        };
?>
</aside>
<?php
    };
?>
<?php 

else :

    _e( 'Sorry, no  posts matched your criteria.', 'textdomain' );

endif;

get_footer();

?>