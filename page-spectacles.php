<?php

get_header();

$detect = detect();

?>
<div id="content" class="spectacles loading">

    <main id="main" class="spectacles__main" role="main">
    <?php

        $content = get_post()->post_content;
        if( !empty( $content ) ) {

            while (  have_posts() ) : the_post();
    
            the_content();
    
            endwhile;

        } else {

            echo !$detect->isMobile() ? '<div class="main__category"><p></p></div>' : null;
            echo $detect->isTablet() ? '<div class="main__category"><p></p></div>' : null;

        }

        $the_query = new WP_Query( array (
            'cat' => 2,
            'meta_key' => 'creation_annee',
            'orderby' => 'meta_value',
            'order' => 'ASC',
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => -1
        ) );

        $i = 0;

        while ( $the_query->have_posts() ) : $the_query->the_post(); $i++; ?>

        <article data-id="<?php echo $i; ?>">
            <a href="<?php the_permalink(); ?>">
            <?php
                $date = get_field( "creation_annee" );
                $date = !empty( $date ) ? $date : date( "Y" );
            ?>
                <h4><?php echo get_the_title().", $date"; ?></h4>
            </a>
        </article>

    <?php endwhile; wp_reset_postdata(); ?>

    </main>

    <aside id="aside" class="spectacles__aside">
<?php
    if ( !$detect->isMobile() ) {
    
        $i = 0;
        while ( $the_query->have_posts() ) : $the_query->the_post(); $i++; ?>

        <figure class="fig-<?php echo $i; ?> hidden">
            <?php the_post_thumbnail( 'medium_large' ); ?>
        </figure>
<?php
        endwhile; wp_reset_postdata();
    };
?>
</aside>
<?php get_footer(); ?>