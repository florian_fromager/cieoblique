<?php

    define( 'ROOTPATH', dirname(__FILE__) );

    add_theme_support( 'post-thumbnails' );

    function scotch() {

        $num = rand( 1, 9 );

        $homeUrl = get_home_url();

        return "<figure id='scotch'><img src='$homeUrl/wp-content/themes/cieoblique/assets/img/scotch/scotch_$num.webp' alt='scotch_$num'></figure>";

    }

    function detect() {
        
        require_once "libs/Mobile_Detect.php";
        $detect = new Mobile_Detect;

        return $detect;

    }

    function scan( $arr ) {
        $filled = false;
        foreach ( $arr as $field ) {
            $filled = empty( $field ) ? true : $filled;
        }
        return $filled;
    }

    function add_editor_setup() {

        add_theme_support( 'editor-styles' );
        add_editor_style( 'assets/css/style-editor.css' );

    }

    add_action( 'after_setup_theme', 'add_editor_setup' );
    
    function theme_support_options() {

        $defaults = array(

            'height'      => 150,
            'width'       => 250,
            'flex-height' => false,
            'flex-width'  => false

        );

        add_theme_support( 'custom-logo', $defaults );
    }
    
    add_action( 'after_setup_theme', 'theme_support_options' );

    function my_add_template_to_posts() {
        $post_type_object = get_post_type_object( 'post' );
        $post_type_object->template = array(
            array( 'core/group', array(), array(
                array( 'core/list', array(
                    'placeholder' => 'Liste: Mise en scène, adaptation, écriture, etc.',
                ) ),
                array( 'core/paragraph', array(
                    'placeholder' => 'Informations supplémentaires',
                ) ),
                array( 'core/list', array(
                    'placeholder' => 'Liste: Création, Durée, Tout',
                ) ),
            ) ),
            array( 'core/group', array(), array(
                array( 'core/columns', array(), array(
                    array( 'core/column', array(), array(
                        array( 'core/paragraph', array(
                            'placeholder' => 'colonne 1',
                        ) ),
                    ) ),
                    array( 'core/column', array(), array(
                        array( 'core/paragraph', array(
                            'placeholder' => 'colonne 2',
                        ) ),
                    ) ),
                    array( 'core/column', array(), array(
                        array( 'core/paragraph', array(
                            'placeholder' => 'colonne 3',
                        ) ),
                    ) ),
                ) ),
            ) ),
            array( 'core/columns', array(), array(
                array( 'core/column', array(), array(
                    array( 'core/paragraph', array(
                        'placeholder' => 'Texte général',
                    ) ),
                ) ),
            ) ),
        );
        $post_type_object->template_lock = 'all';
    }
    add_action( 'init', 'my_add_template_to_posts' );

    // function add_presentation_template() {
    //     $post_type_object = get_post_type_object( 'page' );
    //     $post_type_object->template = array(
    //         array( 'core/heading', array(
    //             'placeholder' => 'Présentation',
    //             'level' => 1,
    //         ) ),
    //         array( 'core/group', array(), array(
    //             array( 'core/heading', array(
    //                 'placeholder' => 'Titre de section',
    //                 'level' => 2,
    //             ) ),
    //             array( 'core/paragraph', array(
    //                 'placeholder' => 'Soustitre',
    //             ) ),
    //             array( 'core/columns', array(), array(
    //                 array( 'core/column', array(), array(
    //                     array( 'core/paragraph', array(
    //                         'placeholder' => 'Texte général',
    //                     ) ),
    //                 ) ),
    //             ) ),
    //         ) ),
    //         array( 'core/group', array(), array(
    //             array( 'core/heading', array(
    //                 'placeholder' => 'Titre de section',
    //                 'level' => 2,
    //             ) ),
    //             array( 'core/paragraph', array(
    //                 'placeholder' => 'Soustitre',
    //             ) ),
    //             array( 'core/columns', array(), array(
    //                 array( 'core/column', array(), array(
    //                     array( 'core/paragraph', array(
    //                         'placeholder' => 'Texte général',
    //                     ) ),
    //                 ) ),
    //             ) ),
    //         ) ),
    //         array( 'core/group', array(), array(
    //             array( 'core/heading', array(
    //                 'placeholder' => 'Titre de section',
    //                 'level' => 2,
    //             ) ),
    //             array( 'core/paragraph', array(
    //                 'placeholder' => 'Soustitre',
    //             ) ),
    //             array( 'core/columns', array(), array(
    //                 array( 'core/column', array(), array(
    //                     array( 'core/paragraph', array(
    //                         'placeholder' => 'Texte général',
    //                     ) ),
    //                 ) ),
    //             ) ),
    //         ) ),
    //         array( 'core/group', array(), array(
    //             array( 'core/heading', array(
    //                 'placeholder' => 'Titre de section',
    //                 'level' => 2,
    //             ) ),
    //             array( 'core/columns', array(), array(
    //                 array( 'core/column', array(), array(
    //                     array( 'core/paragraph', array(
    //                         'placeholder' => 'Colonne de gauche',
    //                         'columns' => 1,
    //                     ) ),
    //                 ) ),
    //                 array( 'core/column', array(), array(
    //                     array( 'core/paragraph', array(
    //                         'placeholder' => 'Colonne de droite',
    //                         'columns' => 3,
    //                     ) ),
    //                 ) ),
    //             ) ),
    //         ) ),
    //     );
    //     $post_type_object->template_lock = 'all'; // Verrouiller la modification
    // }

    // add_action( 'init', 'add_presentation_template' );

    // function create_events() {

    //     register_post_type( 'events',

    //         array(

    //             'labels' => array(
                    
    //                 'name' => __( 'Events' ),
    //                 'singular_name' => __( 'Event' )

    //             ),
    //             'public' => true,
    //             'has_archive' => true,
    //             'rewrite' => array( 'slug' => 'events' ),
    //             'show_in_rest' => true,

    //         ),

    //     );

    // }
    
    // add_action( 'init', 'create_events' );

    class FLHM_HTML_Compression
{
protected $flhm_compress_css = true;
protected $flhm_compress_js = true;
protected $flhm_info_comment = true;
protected $flhm_remove_comments = true;
protected $html;
public function __construct($html)
{
if (!empty($html))
{
$this->flhm_parseHTML($html);
}
}
public function __toString()
{
return $this->html;
}
protected function flhm_bottomComment($raw, $compressed)
{
$raw = strlen($raw);
$compressed = strlen($compressed);
$savings = ($raw-$compressed) / $raw * 100;
$savings = round($savings, 2);
return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
}
protected function flhm_minifyHTML($html)
{
$pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
$overriding = false;
$raw_tag = false;
$html = '';
foreach ($matches as $token)
{
$tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
$content = $token[0];
if (is_null($tag))
{
if ( !empty($token['script']) )
{
$strip = $this->flhm_compress_js;
}
else if ( !empty($token['style']) )
{
$strip = $this->flhm_compress_css;
}
else if ($content == '<!--wp-html-compression no compression-->')
{
$overriding = !$overriding; 
continue;
}
else if ($this->flhm_remove_comments)
{
if (!$overriding && $raw_tag != 'textarea')
{
$content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
}
}
}
else
{
if ($tag == 'pre' || $tag == 'textarea')
{
$raw_tag = $tag;
}
else if ($tag == '/pre' || $tag == '/textarea')
{
$raw_tag = false;
}
else
{
if ($raw_tag || $overriding)
{
$strip = false;
}
else
{
$strip = true; 
$content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content); 
$content = str_replace(' />', '/>', $content);
}
}
} 
if ($strip)
{
$content = $this->flhm_removeWhiteSpace($content);
}
$html .= $content;
} 
return $html;
} 
public function flhm_parseHTML($html)
{
$this->html = $this->flhm_minifyHTML($html);
if ($this->flhm_info_comment)
{
$this->html .= "\n" . $this->flhm_bottomComment($html, $this->html);
}
}
protected function flhm_removeWhiteSpace($str)
{
$str = str_replace("\t", ' ', $str);
$str = str_replace("\n",  '', $str);
$str = str_replace("\r",  '', $str);
$str = str_replace("// The customizer requires postMessage and CORS (if the site is cross domain).",'',$str);
while (stristr($str, '  '))
{
$str = str_replace('  ', ' ', $str);
}   
return $str;
}
}
function flhm_wp_html_compression_finish($html)
{
return new FLHM_HTML_Compression($html);
}
function flhm_wp_html_compression_start()
{
ob_start('flhm_wp_html_compression_finish');
}
add_action('get_header', 'flhm_wp_html_compression_start', 99999);

    function removeEmptyParagraphs($content) {

        $arr = [
            "<p></p>",
            "<li></li>",
            "<ul></ul>",
            "<12></h1>",
            "<h2></h2>",
            "<h3></h3>",
            "<h4></h4>",
            "<h5></h5>",
            "<h6></h6>",
            '<div class="wp-block-column"></div>',
        ];

        foreach ( $arr as $tag ) {
            $content = str_replace( $tag, "", $content );
        }

        return $content;

    }
    
    add_filter( 'the_content', 'removeEmptyParagraphs', 9999);

    add_filter( 'allowed_block_types', function( $block_types, $post ) {

    	$allowed = [

    		'core/paragraph',
    		'core/heading',
    		'core/image',
            'core/list',
            'core/columns',
            'core/column',
    	];

    	if ($post->post_type == 'book') {

    		return $allowed;

    	}
        
    	return $block_types;

    }, 10, 2 );