<?php

get_header();

?>
<div id="content" class="calendrier loading">

    <main id="main" class="calendrier__main" role="main">
    <?php

        $detect = detect();
        $mobile = $detect->isMobile();
        echo !$mobile ? '<div class="main__category"><p></p></div>' : null;
        echo $detect->isTablet() ? '<div class="main__category"><p></p></div>' : null;

        $calendarr = [];

        $the_query = new WP_Query( array (
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => -1
        ) );

        while ( $the_query->have_posts() ) : $the_query->the_post();

            $title = get_the_title();
            $id = get_the_ID();

            $cat = get_the_category();
            $cat = $cat[ 0 ];
            $slug = $cat->slug;

            $events = get_field( $slug."_events" );

            if ( !empty( $events ) ) {

                $events = $events[ "body" ];

                for ( $i = 0; $i < count( $events ); $i++ ) {

                    $event = [];
                    $e = $events[ $i ];

                    $event[ 'id' ] = $id;

                    $debut = empty( $e[ 0 ][ "c" ] ) ? "" : strtotime( $e[ 0 ][ "c" ] );
                    $event[ 'date_debut' ] = $debut;

                    $fin = empty( $e[ 1 ][ "c" ] ) ? "" : strtotime( $e[ 1 ][ "c" ] );
                    $event[ 'date_fin' ] = $fin;

                    $event[ 'date_str' ] = $e[ 2 ][ "c" ];
                    $event[ 'title' ] = empty( $e[ 3 ][ "c" ] ) ? $title : $e[ 3 ][ "c" ];
                    $event[ 'place' ] = $e[ 4 ][ "c" ];
                    $event[ 'hour_str' ] = $e[ 5 ][ "c" ];

                    array_push( $calendarr, $event );

                };

            };

        endwhile; wp_reset_postdata();

        $pieces = [];
        $oldId = null;

        for ( $i = 0; $i < count( $calendarr ); $i++ ) {

            $id = $calendarr[ $i ][ "id" ];
            
            if ( $id != $oldId ) {
                array_push( $pieces, $id );
            }

            $oldId = $id;

        }

        usort( $calendarr, function( $a, $b ) {

            return $a[ 'date_debut' ] <=> $b[ 'date_debut' ];

        } );

        if ( !empty( $calendarr ) ) {

            for ( $i = 0; $i < count( $calendarr ); $i++ ) {

                $e = $calendarr[ $i ];
                $debut = $e[ "date_debut" ];
                $fin = $e[ "date_fin" ];
                $lineBr = !$mobile ? "<br/>" : " ";
                $date = date( "d", $debut );
                $date .= empty( $fin ) ? null : " — ".date( "d", $fin );
                $date .= empty( $fin ) ? " ".date( "F", $debut ).$lineBr.date( "Y", $debut ) : $lineBr.date( "M", $fin ).". ".date( "Y", $fin );
                
    ?>
            <article data-id="<?php echo $e[ "id" ]; ?>">
                <time datetime="<?php echo date( "Y-m-d", $debut ); ?>"><?php echo $date; ?></time>
                <a href="<?php the_permalink( $e[ "id" ] ); ?>">
                    <h4><?php echo $e[ "title" ]; ?></h4>
                    <h5><?php echo $e[ "place" ]; ?></h5>
                    <p><?php echo $e[ "hour_str" ]; ?></p>
                </a>
            </article>
        
        <?php };

        };
        
    ?>

    </main>

    <?php if ( !$mobile ) { ?>
    <aside id="aside" class="calendrier__aside">

    <?php 

        if ( !empty( $calendarr ) ) {

            for ( $i = 0; $i < count( $pieces ); $i++ ) {

                $id = $pieces[ $i ];
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'medium_large');
                
    ?>
        <figure class="fig-<?php echo $id; ?> hidden">
            <img src="<?php echo $thumb[0]; ?>" alt="<?php echo $title; ?>" loading="lazy" srcset="" sizes="(max-width: 768px) 100vw, 768px" width="768" height="1041">
        </figure>
    <?php };

    };
        
    ?>

    </aside>
    <?php } ?>
<?php

    get_footer();

?>