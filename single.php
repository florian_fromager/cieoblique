<?php

    get_header();

	$detect = detect();
    $mobile = $detect->isMobile();
    $tablet = $detect->isTablet();

    $cat = get_the_category();
    $cat = $cat[ 0 ];
    $slug = $cat->slug;
        
    $videos_field = $slug.'_videos';
    $videos = get_field( $videos_field );

    if ( have_posts() ) :

?>
<div id="content" class="<?php echo $slug ?> loading">

    <?php if ( $mobile ) { ?>
    <header class="main__heading">

        <div class="main__category"><p><?php
            $cat = ucfirst( $slug );
            $date = get_field( $slug."_annee" );
            $date = !empty( $date ) ? $date : date( "Y" );

            echo "$cat $date";
        ?></p></div>

        <h1 class="main__title"><?php the_title(); ?></h1>

    </header>
    <?php } ?>
    
    <?php if( $mobile || $tablet ) get_template_part( 'parts/single_slider', null, array( 'slug' => $slug, 'videos' => $videos ) ); ?>

    <main id="main" class="<?php echo $slug ?>__main" role="main">

        <?php if ( !$mobile ) { ?>
        <header class="main__heading">

            <div class="main__category"><p><?php
                $cat = ucfirst( $slug );
                $date = get_field( $slug."_annee" );
                $date = !empty( $date ) ? $date : date( "Y" );

                echo "$cat $date";
            ?></p></div>

            <h1 class="main__title"><?php the_title(); ?></h1>

        </header>
        <?php } ?>

    <?php
        
        while (  have_posts() ) : the_post();

            the_content();

        endwhile;
        
        $annexes_field = $slug.'_annexes';
        $annexes = get_field( $annexes_field );

        if( !empty( $annexes ) ) {
    ?>
        
        <div class="wp-block-group last">
            <div class="wp-block-group__inner-container">
                <ul>
                <?php
                    for ( $i = 0; $i < count( $annexes ); $i++ ) {

                        $annexe = $annexes[ $i ];
                        $title = !empty( $annexe->post_content ) ? $annexe->post_content : $annexe->post_title;
                        $url = $annexe->guid;
                        
                        echo "<li>➔ <a href='$url' target='_blank'>$title</a></li>";
                        
                    }
                ?>
                </ul>
            </div>
        </div>
        
    <?php
        }

        if( scan( $videos ) ) {
            echo "<button id='videos__btn--open' class='btn'>➔ Galerie Vidéo</button>";
        }
    ?>

    </main>
    
    <?php if( !$mobile && !$tablet ) get_template_part( 'parts/single_slider', null, array( 'slug' => $slug, 'videos' => $videos ) ); ?>

<?php 

    else :

        _e( 'Sorry, no  posts matched your criteria.', 'textdomain' );

    endif;

    get_footer();

?>