<?php

get_header();

if ( have_posts() ) :

?>	
<div id="content" class="mentions loading">

    <main id="main" class="mentions__main" role="main">

        <h1><?php the_title(); ?></h1>

    <?php
        while (  have_posts() ) : the_post();

            the_content();

        endwhile;
    ?>

    </main>

    <?php
        $detect = detect();
        if ( !$detect->isMobile() ) echo '<aside id="aside" class="mentions__aside"></aside>';
    ?>
<?php

else :

    _e( 'Sorry, no  posts matched your criteria.', 'textdomain' );

endif;

get_footer();

?>