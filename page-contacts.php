<?php

get_header();

if ( have_posts() ) :

?>	
<div id="content" class="contacts loading">

    <main id="main" class="contacts__main" role="main">
    <?php
        while (  have_posts() ) : the_post();

            the_content();

        endwhile;
    ?>
    </main>

    <?php
        $detect = detect();
        if ( !$detect->isMobile() ) echo '<aside id="aside" class="contacts__aside"></aside>';
    ?>

<?php

else :

    _e( 'Sorry, no  posts matched your criteria.', 'textdomain' );

endif;

get_footer();

?>